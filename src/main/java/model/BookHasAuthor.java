package model;

import annotation.Column;
import annotation.Table;

@Table(name = "book_has_author")
public class BookHasAuthor {

  @Column(name = "book_id")
  private Long bookId;

  @Column(name = "book_id")
  private Long authorId;

  public Long getBookId() {
    return bookId;
  }

  public Long getAuthorId() {
    return authorId;
  }
}
