package model;

import annotation.Column;
import annotation.PrimaryKey;
import annotation.Table;

import java.util.Date;

@Table(name = "book")
public class Book {

    @PrimaryKey
    @Column(name = "id")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "release_date")
    private Date releaseDate;

    public Book() {
    }

    public Book(Long id, String title, String description, String imageUrl, Date releaseDate) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.releaseDate = releaseDate;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", releaseDate=" + releaseDate +
                '}';
    }
}
