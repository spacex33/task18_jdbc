package model;

import annotation.Column;
import annotation.PrimaryKey;
import annotation.Table;

@Table(name = "review")
public class Review {

  @PrimaryKey
  @Column(name = "id")
  private Long id;

  @Column(name = "stars_num")
  private Long starsNum;

  @Column(name = "comment")
  private String comment;

  @Column(name = "book_id")
  private Long bookId;

  public Long getId() {
    return id;
  }

  public Long getStarsNum() {
    return starsNum;
  }

  public String getComment() {
    return comment;
  }

  public Long getBookId() {
    return bookId;
  }
}
