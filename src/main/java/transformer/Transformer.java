package transformer;

import annotation.Column;
import annotation.Table;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Transformer {

    private Transformer(){}

    public static <T> T resultSetToEntity(ResultSet rs, Class<T> clazz)
            throws SQLException {
        Object entity = null;
        try {
            entity = clazz.getConstructor().newInstance();
            if (clazz.isAnnotationPresent(Table.class)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Column.class)) {
                        Column column = field.getAnnotation(Column.class);
                        String tableColumnName = column.name();
                        field.setAccessible(true);
                        Class fieldType = field.getType();

                        if (fieldType == String.class) {
                            field.set(entity, rs.getString(tableColumnName));
                        } else if (fieldType == Integer.class) {
                            field.set(entity, rs.getInt(tableColumnName));
                        } else if (fieldType == Date.class) {
                            field.set(entity, new Date(rs.getDate(tableColumnName).getTime()));
                        } else if(fieldType == Long.class) {
                            field.set(entity, (long) rs.getInt(tableColumnName));
                        }
                    }
                }
            }
        } catch (InstantiationException
                | IllegalAccessException
                | InvocationTargetException
                | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return (T)entity;
    }

}
