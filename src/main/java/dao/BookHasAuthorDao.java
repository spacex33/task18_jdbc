package dao;

import model.BookHasAuthor;
import persistent.ConnectionManager;
import transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookHasAuthorDao implements Dao<BookHasAuthor> {

    private static final String FIND_ALL = "SELECT * FROM book_has_author";
    private static final String SAVE = "INSERT book_has_author (book_id, author_id) VALUES (?, ?)";

    @Override
    public Optional<BookHasAuthor> get(long id) {
        return null;
    }

    @Override
    public List<BookHasAuthor> getAll() {
        List<BookHasAuthor> bookHasAuthors = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                bookHasAuthors.add(Transformer.resultSetToEntity(resultSet, BookHasAuthor.class));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bookHasAuthors;
    }

    @Override
    public void save(BookHasAuthor bookHasAuthor) {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(SAVE)) {
            ps.setString(1, String.valueOf(bookHasAuthor.getBookId()));
            ps.setString(2, String.valueOf(bookHasAuthor.getAuthorId()));
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(BookHasAuthor bookHasAuthor) {

    }

    @Override
    public void delete(long id) {

    }
}
