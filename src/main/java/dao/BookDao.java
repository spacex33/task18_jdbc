package dao;

import model.Book;
import persistent.ConnectionManager;
import transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookDao implements Dao<Book> {

    private static final String FIND_ALL = "SELECT * FROM book";
    private static final String FIND_BY_ID = "SELECT * FROM book WHERE id=?";
    private static final String SAVE = "INSERT book (id, title, description, image_url, release_date) " +
            "VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE book SET title=?, description=?, image_url=?, " +
            "release_date=? WHERE id=?";
    private static final String DELETE = "DELETE FROM book WHERE id=?";

    public List<Book> getAll() {
        List<Book> books = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                books.add(Transformer.resultSetToEntity(resultSet, Book.class));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return books;
    }

    @Override
    public Optional<Book> get(long id) {
        Book book = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, String.valueOf(id));
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                book = Transformer.resultSetToEntity(resultSet, Book.class);
                break;
            }
        } catch (SQLException e) {
            // then returns book as optional
        }
        return Optional.ofNullable(book);
    }

    @Override
    public void save(Book book) {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(SAVE)) {
            ps.setString(1, String.valueOf(book.getId()));
            ps.setString(2, book.getTitle());
            ps.setString(3, book.getDescription());
            ps.setString(4, book.getImageUrl());
            ps.setString(5, new Date(book.getReleaseDate().getTime()).toString());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Book book) {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1, book.getTitle());
            ps.setString(2, book.getDescription());
            ps.setString(3, book.getImageUrl());
            ps.setString(4, new Date(book.getReleaseDate().getTime()).toString());
            ps.setString(5, String.valueOf(book.getId()));
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(long id) {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setString(1, String.valueOf(id));
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
