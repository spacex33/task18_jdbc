package dao;

import model.Review;
import persistent.ConnectionManager;
import transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ReviewDao implements Dao<Review> {

    private static final String FIND_ALL = "SELECT * FROM review";
    private static final String FIND_BY_ID = "SELECT * FROM review WHERE id=?";
    private static final String SAVE = "INSERT review (id, stars_num, comment, book_id) " +
            "VALUES (?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE review SET stars=?, comment=?, book_id=? " +
            "WHERE id=?";
    private static final String DELETE = "DELETE FROM review WHERE id=?";

    @Override
    public Optional<Review> get(long id) {
        Review review = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, String.valueOf(id));
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                review = Transformer.resultSetToEntity(resultSet, Review.class);
                break;
            }
        } catch (SQLException e) {
            // then returns author as optional
        }
        return Optional.ofNullable(review);
    }

    @Override
    public List<Review> getAll() {
        List<Review> reviews = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                reviews.add(Transformer.resultSetToEntity(resultSet, Review.class));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reviews;
    }

    @Override
    public void save(Review review) {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(SAVE)) {
            ps.setString(1, String.valueOf(review.getId()));
            ps.setString(2, String.valueOf(review.getStarsNum()));
            ps.setString(3, review.getComment());
            ps.setString(4, String.valueOf(review.getBookId()));
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Review review) {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1, String.valueOf(review.getStarsNum()));
            ps.setString(2, review.getComment());
            ps.setString(3, String.valueOf(review.getBookId()));
            ps.setString(4, String.valueOf(review.getId()));
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(long id) {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setString(1, String.valueOf(id));
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
