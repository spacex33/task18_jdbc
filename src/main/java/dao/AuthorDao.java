package dao;

import model.Author;
import persistent.ConnectionManager;
import transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AuthorDao implements Dao<Author> {

    private static final String FIND_ALL = "SELECT * FROM author";
    private static final String FIND_BY_ID = "SELECT * FROM author WHERE id=?";
    private static final String SAVE = "INSERT author (id, name) VALUES (?, ?)";
    private static final String UPDATE = "UPDATE author SET name=?  WHERE id=?";
    private static final String DELETE = "DELETE FROM author WHERE id=?";

    @Override
    public Optional<Author> get(long id) {
        Author author = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, String.valueOf(id));
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                author = Transformer.resultSetToEntity(resultSet, Author.class);
                break;
            }
        } catch (SQLException e) {
            // then returns author as optional
        }
        return Optional.ofNullable(author);
    }

    @Override
    public List<Author> getAll() {
        List<Author> authors = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(FIND_ALL);
            while (resultSet.next()) {
                authors.add(Transformer.resultSetToEntity(resultSet, Author.class));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return authors;
    }

    @Override
    public void save(Author author) {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(SAVE)) {
            ps.setString(1, String.valueOf(author.getId()));
            ps.setString(2, author.getName());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Author author) {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1, author.getName());
            ps.setString(2, String.valueOf(author.getId()));
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(long id) {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setString(1, String.valueOf(id));
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
