package dao;

import model.metadata.ColumnMetaData;
import model.metadata.ForeignKeyMetaData;
import model.metadata.TableMetaData;
import persistent.ConnectionManager;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MetaDataDao {

    public List<String> findAllTableName() {
        List<String> tableNames = new ArrayList<>();
        String[] types = {"TABLE"};
        try {
            Connection connection = ConnectionManager.getConnection();
            DatabaseMetaData databaseMetaData = connection.getMetaData();
            ResultSet result = databaseMetaData.getTables(connection.getCatalog(), null, "%", types);

            while (result.next()) {
                String tableName = result.getString("TABLE_NAME");
                tableNames.add(tableName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tableNames;
    }

    public List<TableMetaData> getTablesStructure()  {
        List<TableMetaData> tableMetaDataList = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try {
            DatabaseMetaData databaseMetaData = connection.getMetaData();

            String[] types = {"TABLE"};
            String dbName = connection.getCatalog();
            ResultSet result = databaseMetaData.getTables(dbName, null, "%", types);

            while (result.next()) {
                String tableName = result.getString("TABLE_NAME");
                TableMetaData tableMetaData = new TableMetaData();
                tableMetaData.setDBName(dbName);
                tableMetaData.setTableName(tableName);

                //Get Primary Keys
                List<String> pkList = new ArrayList<>();
                ResultSet PKs = databaseMetaData.getPrimaryKeys(connection.getCatalog(), null, tableName);
                while (PKs.next()) {
                    pkList.add(PKs.getString("COLUMN_NAME"));
                }

                //Get Columns For Table
                List<ColumnMetaData> columnsMetaData = new ArrayList<>();
                ResultSet columnsRS = databaseMetaData.getColumns(dbName, null, tableName, "%");
                while (columnsRS.next()) {
                    ColumnMetaData columnMetaData = new ColumnMetaData();
                    columnMetaData.setColumnName(columnsRS.getString("COLUMN_NAME"));
                    columnMetaData.setDataType(columnsRS.getString("TYPE_NAME"));
                    columnMetaData.setColumnSize(columnsRS.getString("COLUMN_SIZE"));
                    columnMetaData.setDecimalDigits(columnsRS.getString("DECIMAL_DIGITS"));
                    boolean cond = columnsRS.getString("IS_NULLABLE").equals("YES");
                    columnMetaData.setNullable(cond);
                    cond = columnsRS.getString("IS_AUTOINCREMENT").equals("IS_AUTOINCREMENT");
                    columnMetaData.setAutoIncrement(cond);

                    columnMetaData.setPrimaryKey(false);
                    for (String pkName : pkList) {
                        if (columnMetaData.getColumnName().equals(pkName)) {
                            columnMetaData.setPrimaryKey(true);
                            break;
                        }
                    }
                    columnsMetaData.add(columnMetaData);
                }
                tableMetaData.setColumnMetaData(columnsMetaData);

                //Get Foreign Keys
                List<ForeignKeyMetaData> fkMetaDataList = new ArrayList<>();
                ResultSet FKsRS = databaseMetaData.getImportedKeys(dbName, null, tableName);
                while (FKsRS.next()) {
                    ForeignKeyMetaData fk = new ForeignKeyMetaData();
                    fk.setFkColumnName(FKsRS.getString("FKCOLUMN_NAME"));
                    fk.setPkTableName(FKsRS.getString("PKTABLE_NAME"));
                    fk.setPkColumnName(FKsRS.getString("PKCOLUMN_NAME"));
                    fkMetaDataList.add(fk);
                }
                tableMetaData.setForeignKeyList(fkMetaDataList);

                tableMetaDataList.add(tableMetaData);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return tableMetaDataList;
    }
}
