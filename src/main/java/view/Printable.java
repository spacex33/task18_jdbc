package view;

@FunctionalInterface
interface Printable {
    void print();
}