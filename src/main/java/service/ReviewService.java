package service;

import dao.Dao;
import model.Review;

import java.util.List;

public class ReviewService {

    private Dao<Review> reviewDao;

    public ReviewService(Dao<Review> reviewDao) {
        this.reviewDao = reviewDao;
    }

    public List<Review> findAll() {
        return reviewDao.getAll();
    }

    public Review findById(long id) {
        return reviewDao.get(id).orElseThrow(NullPointerException::new);
    }

    public void save(Review review) {
        reviewDao.save(review);
    }

    public void update(Review review) {
        reviewDao.update(review);
    }

    public void delete(long id) {
        reviewDao.delete(id);
    }
}
