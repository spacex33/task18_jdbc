package service;

import dao.Dao;
import model.Author;

import java.util.List;

public class AuthorService {

    private Dao<Author> authorDao;

    public AuthorService(Dao<Author> authorDao) {
        this.authorDao = authorDao;
    }

    public List<Author> findAll() {
        return authorDao.getAll();
    }

    public Author findById(long id) {
        return authorDao.get(id).orElseThrow(NullPointerException::new);
    }

    public void save(Author author) {
        authorDao.save(author);
    }

    public void update(Author author) {
        authorDao.update(author);
    }

    public void delete(long id) {
        authorDao.delete(id);
    }
}
