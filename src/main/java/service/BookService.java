package service;

import dao.Dao;
import model.Book;

import java.util.List;

public class BookService {

    private Dao<Book> bookDao;

    public BookService(Dao<Book> bookDao) {
        this.bookDao = bookDao;
    }

    public List<Book> findAll() {
        return bookDao.getAll();
    }

    public Book findById(long id) {
        return bookDao.get(id).orElseThrow(NullPointerException::new);
    }

    public void save(Book book) {
        bookDao.save(book);
    }

    public void update(Book book) {
        bookDao.update(book);
    }

    public void delete(long id) {
        bookDao.delete(id);
    }
}
