package service;

import dao.MetaDataDao;
import model.metadata.TableMetaData;

import java.sql.SQLException;
import java.util.List;

public class MetaDataService {

    public List<String> findAllTableName() {
        return new MetaDataDao().findAllTableName();
    }

    public List<TableMetaData> getTablesStructure() {
        return new MetaDataDao().getTablesStructure();
    }
}
