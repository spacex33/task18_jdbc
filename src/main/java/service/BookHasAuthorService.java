package service;

import dao.Dao;
import model.BookHasAuthor;

import java.util.List;

public class BookHasAuthorService {

    private Dao<BookHasAuthor> bookHasAuthorDao;

    public BookHasAuthorService(Dao<BookHasAuthor> bookHasAuthorDao) {
        this.bookHasAuthorDao = bookHasAuthorDao;
    }

    public List<BookHasAuthor> findAll() {
        return bookHasAuthorDao.getAll();
    }

    public BookHasAuthor findById(long id) {
        return bookHasAuthorDao.get(id).orElseThrow(NullPointerException::new);
    }

    public void save(BookHasAuthor bookHasAuthor) {
        bookHasAuthorDao.save(bookHasAuthor);
    }

    public void update(BookHasAuthor bookHasAuthor) {
        bookHasAuthorDao.update(bookHasAuthor);
    }

    public void delete(long id) {
        bookHasAuthorDao.delete(id);
    }
}
